import express from 'express'
import * as bodyParser from 'body-parser'

require('dotenv').config()

const app = express()
app.use(bodyParser.json())

// Express configuration
app.set('port', process.env.BACK_PORT)

export default app

module.exports = app
