import errorHandler from 'errorhandler'
import { createConnection } from 'typeorm'
import http from 'http'
import express from 'express'

interface MyExpress extends express.Express {
  server: http.Server
}

const app: MyExpress = require('./app')

/**
 * Error Handler. Provides full stack - remove for production
 */
app.use(errorHandler())

module.exports = {
  path: '/api',
  handler: app
}
